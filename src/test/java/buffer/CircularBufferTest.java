package buffer;

import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;

import java.nio.Buffer;

/**
 * Zadanie 2
 *
 * Bufor cykliczny jest to struktura danych składająca się z tablicy o pewnym z góry ustalonym, maksymalnym rozmiarze oraz zmiennych przechowujących dwa indeksy
 * – startowy (głowa ang. head) oraz końcowy (ogon ang. tail).
 * Głowa wskazuje pierwszy element, który zostanie odczytany z bufora.
 * Akcja odczytu przesuwa indeks na kolejny element. Ogon zaś wskazuje na ostatni element jaki został zapisany.
 * Kolejna akcja zapisu przesuwa indeks na kolejny element.
 * Pierwszy element do odczytu nie musi znajdować się na początku bufora.
 *
 * !!! Zwróć uwagę na dwa zagadnienia: obsługa dojścia do tablicy oraz zjawisko przepełnienia.
 */
class CircularBufferTest implements WithAssertions {
    @Test
    void sizeBufferTest() {
        CircularBuffer buffer = new CircularBuffer(8);
        assertThat(buffer.getMaxSize()).isEqualTo(8);
    }

    @Test
    void writeTailTest() {
        CircularBuffer buffer = new CircularBuffer(5);
        buffer.write(3);
        assertThat(buffer.getTailPosition()).isEqualTo(0);
    }

    @Test
    void readHeadTest() {
        CircularBuffer buffer = new CircularBuffer(6);
        buffer.write(3);
        buffer.write(6);
        int value = buffer.read();

        assertThat(value).isEqualTo(3); 
        assertThat(buffer.getHeadPosition()).isEqualTo(1);
    }

    @Test
    void overflowTest(){
        CircularBuffer buffer = new CircularBuffer(3);
        buffer.write(2);
        buffer.write(2);
        buffer.write(2);

        buffer.read();
        buffer.read();
        buffer.read();
        buffer.read();

        assertThat(buffer.getHeadPosition()).isEqualTo(1);
    }

    @Test
    void overflowWriteTest() {
        CircularBuffer buffer = new CircularBuffer(2);

        try {
            buffer.write(2);
            buffer.write(3);
            buffer.write(4);
        } catch (BufferOverflowException ex) {
            assertThat(ex).isInstanceOf(BufferOverflowException.class);
        }

        fail("Powinno rzucać wyjątek");
    }
}