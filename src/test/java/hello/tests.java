package hello;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class tests {

    @Test
    void multiply(){
        Calculator cal = new Calculator();
        Assertions.assertEquals(2,cal.multiply(1,2));
    }

    @Test
    void multiply2(){
        Calculator cal = new Calculator();
        Assertions.assertEquals(9,cal.multiply(3,3));
    }



    @Test
    void divide(){
        Calculator cal = new Calculator();
        Assertions.assertEquals(1,cal.divide(2,2));
    }

    @Test
    void divide2(){
        Calculator cal = new Calculator();
        Assertions.assertNotEquals(2,cal.divide(2,2));
    }
}
