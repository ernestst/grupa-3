package hello;

public class Bajtek {
    private int iq;
    private boolean isInOffice;

    public Bajtek() {
        this.iq = 10;
        this.isInOffice = true;
    }

    public void decreaseIQ() {
        this.iq--;
    }

    public void readBook() {
        this.iq++;
    }

    public void goToWork() {
        this.isInOffice = true;
    }

    public void goToHome() {
        this.isInOffice = false;
    }
}
