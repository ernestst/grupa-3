package hello;

public class Calculator {
    public double multiply(int a, int b) {
        return a * b;
    }

    public double divide(int a, int b) {
        return a / b;
    }

    public int add(int a, int b){
        return a+b;
    }

    public int minus(int a, int b){
        return a-b;
    }
}
