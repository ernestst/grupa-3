package buffer;

public class CircularBuffer {
    private int [] values;
    private int size;

    private int headPosition, tailPosition;

    public CircularBuffer(int size) {
        this.size = size;
        values = new int[size];

        headPosition = 0;
        tailPosition = -1;
    }

    public int getMaxSize() {
        return size;
    }

    public void write(int value) {
        tailPosition++;
        values[tailPosition] = value;
    }

    public int read() {
        if(headPosition == getMaxSize()) {
            headPosition = 0;
            return read();
        }

        int value =  getHeadPosition();
        headPosition++;

        return values[value];
    }

    public int getTailPosition() {
        return tailPosition;
    }

    public int getHeadPosition() {
        return headPosition;
    }
}
